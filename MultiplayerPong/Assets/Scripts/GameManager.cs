﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class GameManager : NetworkBehaviour
{
    [SerializeField]
    private TextMesh _messageTextMesh, _leftScoreText, _rightScoreText;

    [SerializeField]
    private BallLaunch _ballLauncher;

    [SerializeField]
    private HostClient _hostClient;

    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameManager>();
            return _instance;
        }
    }

    private int _playerNumber = 2;

    [SyncVar]
    private int _playerCount = 0;

    public int PlayerCount { get { return _playerCount; } private set { _playerCount = value; } }

    [SyncVar(hook = "UpdateLeftScore")]
    private int _leftScore = 0;

    [SyncVar(hook = "UpdateRightScore")]
    private int _rightScore = 0;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        StartCoroutine(GameLoopRoutine());
    }

    public void AddPlayer(PlayerSetup player)
    {
        PlayerCount++;
    }

    public void RemovePlayer(PlayerSetup player)
    {
        PlayerCount--;
    }

    private IEnumerator GameLoopRoutine()
    {
        yield return EnterLobby();
        yield return StartGame();
        yield return GamePlaying();
        yield return EndGame();
    }

    private IEnumerator EnterLobby()
    {
        _hostClient.DisableBtns();
        _messageTextMesh.text = "Waiting for players";
        _rightScoreText.gameObject.SetActive(false);
        _leftScoreText.gameObject.SetActive(false);

        while (PlayerCount < _playerNumber)
        {
            yield return null;
        }
    }

    private IEnumerator StartGame()
    {
        _messageTextMesh.text = "Game Starting in... 3";
        float timer = 3;
        while (timer > 0)
        {
            _messageTextMesh.text = "Game Starting in... " + (int)timer;
            timer -= Time.deltaTime;
            yield return null;
        }
    }

    private IEnumerator GamePlaying()
    {
        _messageTextMesh.text = string.Empty;
        _ballLauncher.LaunchBall();
        _rightScoreText.gameObject.SetActive(true);
        _leftScoreText.gameObject.SetActive(true);

        while (PlayerCount == 2)
        {
            yield return null;
        }
    }

    private IEnumerator EndGame()
    {
        _ballLauncher.Destroy();
        _messageTextMesh.text = "Game Finalized";
        yield return null;
    }

    public void RightGoal()
    {
        _rightScore++;
    }

    public void LeftGoal()
    {
        _leftScore++;
    }

    private void UpdateLeftScore(int score)
    {
        _leftScoreText.text = score.ToString();
    }

    private void UpdateRightScore(int score)
    {
        _rightScoreText.text = score.ToString();
    }
}