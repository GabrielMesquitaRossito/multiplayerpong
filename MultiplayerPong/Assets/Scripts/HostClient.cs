﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class HostClient : MonoBehaviour
{
    [SerializeField]
    private Button _hostBtn, _clietnBtn;

    private NetworkManager _net { get { return GetComponent<NetworkManager>(); } }

    // Use this for initialization
    private void Start()
    {
        _hostBtn.onClick.AddListener(() => _net.StartHost());
        _clietnBtn.onClick.AddListener(() => _net.StartClient());
    }

    public void DisableBtns()
    {
        _hostBtn.gameObject.SetActive(false);
        _clietnBtn.gameObject.SetActive(false);
    }
}