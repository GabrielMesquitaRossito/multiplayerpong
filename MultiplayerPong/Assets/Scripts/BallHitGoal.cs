﻿using UnityEngine;
using UnityEngine.Networking;

public class BallHitGoal : NetworkBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "RightGoal")
            CmdRightGoal();
        if (collision.gameObject.tag == "LeftGoal")
            CmdLeftGoal();
    }

    [Command]
    private void CmdLeftGoal()
    {
        GameManager.Instance.LeftGoal();
    }

    [Command]
    private void CmdRightGoal()
    {
        GameManager.Instance.RightGoal();
    }
}