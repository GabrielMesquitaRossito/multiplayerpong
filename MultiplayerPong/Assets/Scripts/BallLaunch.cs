﻿using UnityEngine;
using UnityEngine.Networking;

public class BallLaunch : NetworkBehaviour
{
    [SerializeField]
    private GameObject _ballPrefab;

    private Rigidbody2D _rigidBody { get { return _ball.GetComponent<Rigidbody2D>(); } }
    private GameObject _ball;

    public void LaunchBall()
    {
        CmdLauncBall();
    }

    public void Destroy()
    {
        CmdDestroy();
    }

    [Command]
    private void CmdLauncBall()
    {
        if (_ball == null)
        {
            _ball = Instantiate(_ballPrefab);
            NetworkServer.Spawn(_ball);
        }
        CmdAddForce();
    }

    [Command]
    private void CmdAddForce()
    {
        _rigidBody.AddForce(Random.insideUnitCircle.normalized * 1000, ForceMode2D.Force);
    }

    [Command]
    private void CmdDestroy()
    {
        Destroy(_ball);
        _ball = null;
    }
}