﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerSetup : NetworkBehaviour
{
    [SyncVar(hook = "SetPlayerNumber")]
    private int _playerNumber;

    public int PlayerNumber { get { return _playerNumber; } private set { _playerNumber = value; } }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();

        CmdSetupPlayer();
    }

    public override void OnNetworkDestroy()
    {
        base.OnNetworkDestroy();
    }

    private void SetPlayerNumber(int number)
    {
        if (number == 1)
            transform.position = new Vector3(-9, 0, 0);
        else
            transform.position = new Vector3(9, 0, 0);
    }

    [Command]
    private void CmdSetupPlayer()
    {
        GameManager.Instance.AddPlayer(this);
        PlayerNumber = GameManager.Instance.PlayerCount;
    }

    [Command]
    private void CmdPlayerDestroyed()
    {
        GameManager.Instance.RemovePlayer(this);
    }
}