﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

public class PlayerMovement : NetworkBehaviour, IDragHandler
{
    private Vector3 _posHandler;

    public void OnDrag(PointerEventData eventData)
    {
        if (!isLocalPlayer)
            return;

        _posHandler = transform.position;
        _posHandler.y = eventData.pointerCurrentRaycast.worldPosition.y;
        transform.position = _posHandler;
    }
}